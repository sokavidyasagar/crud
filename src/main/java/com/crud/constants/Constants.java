package com.crud.constants;

public class Constants {

	public static final String MSG_RESPONSE_SUCCESS = "Successful operation";
	public static final String MSG_RESPONSE_BAD_REQUEST = "Bad Request";

	public static final int HTTP_RESPONSE_SUCCESS = 200;
	public static final int HTTP_RESPONSE_BAD_REQUEST = 400;
	
}
