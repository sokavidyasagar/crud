package com.crud.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crud.controller.StudentController;
import com.crud.exception.StudentNotFound;
import com.crud.model.Student;
import com.crud.repository.StudentRepository;

@Service
public class StudentServiceImpl implements StudentService {

	private static final Logger LOGGER = LoggerFactory.getLogger(StudentServiceImpl.class);
	
	@Autowired
	private StudentRepository repo;

	@Override
	public List<Student> getAllStudents() {
		LOGGER.info("StudentServiceImpl ::: Getting all students:");
		List<Student> list = (List<Student>) repo.findAll();
		if(list.isEmpty())
			throw new StudentNotFound();
		return (List<Student>) repo.findAll();
	}

	@Override
	public Student getStudent(Integer studentId) {
		LOGGER.info("StudentServiceImpl ::: Getting Single students:");
		Optional<Student> student = repo.findById(studentId);
		Student stu = null;
		if (student.isPresent()) {
			stu = student.get();
			LOGGER.info("StudentServiceImpl :: Student information from DB :: {}", stu);
		}else throw new StudentNotFound(studentId);
		return stu;
	}

	@Override
	public Student saveStudent(Student student) {
		LOGGER.info("StudentServiceImpl ::: Getting Save student:");
		return repo.save(student);
	}

	@Override
	public String deleteStudent(Integer id) {
		LOGGER.info("StudentServiceImpl ::: Getting delete student:");
		Optional<Student> student = repo.findById(id);
		if (student.isPresent()) {
			repo.deleteById(id);
			LOGGER.info("StudentServiceImpl :: Student information from DB ::");
		} else {
			 throw new StudentNotFound(id);
		}
		return "Succesfuly Deleted";
	}

	@Override
	public String updateStudent(Student student) {
		LOGGER.info("StudentServiceImpl ::  updateStudent ::");
		Optional<Student> student1 = repo.findById(student.getId());
		if (student1.isPresent()) {
			repo.save(student);
		} else {
			throw new StudentNotFound(student.getId());
		}
		return "Student info updated successfully";
	}

}
