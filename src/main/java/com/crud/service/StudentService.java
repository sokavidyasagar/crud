package com.crud.service;

import java.util.List;

import com.crud.model.Student;


public interface StudentService {

	public List<Student> getAllStudents();
	
	public Student getStudent(Integer studentId);
	
	public Student saveStudent(Student student);
	
	public String deleteStudent(Integer id);
	
	public String updateStudent(Student student);
}
