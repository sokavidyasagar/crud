package com.crud.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class StudentExceptionController extends ResponseEntityExceptionHandler{
	
	@ExceptionHandler(StudentNotFound.class)
    public ResponseEntity<CustomeErrorReponse> customHandleNotFound(Exception ex, WebRequest request) {

		CustomeErrorReponse errors = new CustomeErrorReponse();
        errors.setError(ex.getMessage());
        errors.setStatus(HttpStatus.NOT_FOUND.value());

        return new ResponseEntity<>(errors, HttpStatus.NOT_FOUND);
    }

}
