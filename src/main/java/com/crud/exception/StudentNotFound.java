package com.crud.exception;

public class StudentNotFound extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public StudentNotFound(int id) {
		super("student id not found : " + id);
	}
	
	public StudentNotFound() {
		super("no students available");
	}

}
