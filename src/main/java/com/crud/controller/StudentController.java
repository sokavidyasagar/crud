package com.crud.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.crud.constants.Constants;
import com.crud.model.Student;
import com.crud.service.StudentService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

@RestController
@Api(value = "student", tags = { "Student"}, description = "The student API")
public class StudentController {
	private static final Logger LOGGER = LoggerFactory.getLogger(StudentController.class);
	@Autowired
	private StudentService service;

	ResponseEntity<List<Student>> respListOfStudents;
	ResponseEntity<Student> respSingleStudent;

	
	@ApiOperation(value = "Get all available student list", notes = "Service for getting all student available in system", response = Student.class, responseContainer = "List", tags = {"Student"})
	@ApiResponses(value = {
			@ApiResponse(code = Constants.HTTP_RESPONSE_SUCCESS, message = Constants.MSG_RESPONSE_SUCCESS, response = Student.class),
			@ApiResponse(code = Constants.HTTP_RESPONSE_BAD_REQUEST, message = Constants.MSG_RESPONSE_BAD_REQUEST, response = Student.class) })
	@GetMapping(value = "/student/getAllStudents", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Student>> getAllStudents() {
		LOGGER.info("StudentController :: getAllStudents");
		respListOfStudents = new ResponseEntity<List<Student>>(service.getAllStudents(), HttpStatus.OK);
		LOGGER.info("StudentController :: getAllStudents {}",respListOfStudents);
		return respListOfStudents;
	}

	
	@ApiOperation(value = "Get student", notes = "Service for getting student available in system", response = Student.class, responseContainer = "Studetnt", tags = {"Student"})
	@ApiResponses(value = {
			@ApiResponse(code = Constants.HTTP_RESPONSE_SUCCESS, message = Constants.MSG_RESPONSE_SUCCESS, response = Student.class),
			@ApiResponse(code = Constants.HTTP_RESPONSE_BAD_REQUEST, message = Constants.MSG_RESPONSE_BAD_REQUEST, response = Student.class) })
	@GetMapping(value = "/student/getStudent/{studentId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Student> getStudent(@PathVariable("studentId") Integer studentId) {
		LOGGER.info("StudentController :: getStudent");
		respSingleStudent = new ResponseEntity<Student>(service.getStudent(studentId), HttpStatus.OK);
		return respSingleStudent;
	}

	
	@ApiOperation(value = "Save Student", notes = "Service for saveStudent into System", response = Student.class, responseContainer = "Student", tags = {"Student"})
	@ApiResponses(value = {
			@ApiResponse(code = Constants.HTTP_RESPONSE_SUCCESS, message = Constants.MSG_RESPONSE_SUCCESS, response = Student.class),
			@ApiResponse(code = Constants.HTTP_RESPONSE_BAD_REQUEST, message = Constants.MSG_RESPONSE_BAD_REQUEST, response = Student.class) })
	@PostMapping(value = "/student/saveStudent", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Student> saveStudent(@RequestBody Student student) {
		LOGGER.info("StudentController :: saveStudent");
		respSingleStudent = new ResponseEntity<Student>(service.saveStudent(student), HttpStatus.OK);
		return respSingleStudent;
	}

	@ApiOperation(value = "Delete Student", notes = "Service for delete Student into System", response = String.class, responseContainer = "Student", tags = {"Student"})
	@ApiResponses(value = {
			@ApiResponse(code = Constants.HTTP_RESPONSE_SUCCESS, message = Constants.MSG_RESPONSE_SUCCESS, response = String.class),
			@ApiResponse(code = Constants.HTTP_RESPONSE_BAD_REQUEST, message = Constants.MSG_RESPONSE_BAD_REQUEST, response = String.class) })
	@DeleteMapping(value = "/student/deleteStudent/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> deleteStudent(@PathVariable("id") Integer id) {
		LOGGER.info("StudentController :: deleteStudent");
		return new ResponseEntity<String>(service.deleteStudent(id), HttpStatus.OK);
	}
	
	
	@ApiOperation(value = "Update Student", notes = "Service for update Student into System", response = Student.class, responseContainer = "Student", tags = {"Student"})
	@ApiResponses(value = {
			@ApiResponse(code = Constants.HTTP_RESPONSE_SUCCESS, message = Constants.MSG_RESPONSE_SUCCESS, response = Student.class),
			@ApiResponse(code = Constants.HTTP_RESPONSE_BAD_REQUEST, message = Constants.MSG_RESPONSE_BAD_REQUEST, response = Student.class) })
	@PostMapping(value = "/student/updateStudent", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> updateStudent(@RequestBody Student student) {
		LOGGER.info("StudentController :: deleteStudent");
		return new ResponseEntity<String>(service.updateStudent(student), HttpStatus.OK);
	}
}
